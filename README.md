# Welcome to AC Back-end!

![](https://media.giphy.com/media/xTiIzJSKB4l7xTouE8/giphy.gif)

În primul și primul rând am vrea să începem prin a vă mulțumi pentru faptul că sunteți astăzi aici și să vă felicităm pentru toată munca pe care ați depus-o până acum în cadrul întregului proces de recrutare. **Well done!**

![](https://media.giphy.com/media/mGK1g88HZRa2FlKGbz/giphy.gif)

Acum, pentru ceea ce urmează, vă invităm într-o nouă aventură marca SiSC. Voia bună, distracția și momentele frumoase sunt garantate în această călătorie, deci tot ce mai rămâne de făcut este să verificăm că totul este pregătit pentru drumul pe care urmează să îl parcurgem.
**Așa că hai să începem aventura!**

![](https://media.giphy.com/media/8m7FhQS9nPHXChu21U/giphy.gif)

Pentru început, după cum probabil ați realizat, **Git** este o unealtă de bază pentru noi. Aici ajung toate proiectele noastre și de aici pleacă mai departe toate creațiile către publicul larg.
Pentru că ne oferă posibilitatea să menținem și să lucrăm întotdeauna pe ultima versiune a fișierelor, acesta aduce un avantaj enorm unei **echipe** care colaborează constant, deci o să vă rugăm și pe voi să îi acordați la fel de multă importanță și să fiți atenți în momentul în care vom explica funcționalitățile de bază.

![](https://media.giphy.com/media/evB90wPnh5LxG3XU5o/giphy.gif)

Puțin mai jos vom lista și **tutorialele + programele** pe care vi le-am pus la dispoziție acum câteva zile, în cazul în care va fi nevoie să le accesați ceva mai rapid:

_Tutoriale:_

- [**JavaScript basics**](https://www.codecademy.com/learn/introduction-to-javascript)
- **[SQL basics](https://www.edureka.co/blog/sql-basics/)**
- **[SQL basics video](https://www.youtube.com/watch?v=bEtnYWuo2Bw)**
- **[NodeJS basics](https://www.youtube.com/watch?v=U8XF6AFGqlc)**

_Programe de instalat:_

- **[Laragon](https://laragon.org/download/index.html)**
- **[Xampp](https://www.apachefriends.org/ro/download.html) (pentru cei cu MacOS)**
  `Precizare: dacă îl folosiți pe Windows, nu îl instalați în C:\Program files`
- **[Visual Studio Code](https://code.visualstudio.com/)**
- **[NodeJS](https://nodejs.org/en/)**
- **[Git](https://git-scm.com/downloads)**
`Precizare: să bifați căsuța cu Git Bash când apare`

![](https://media.giphy.com/media/Lm63QU87HvgVEuTV63/giphy.gif)

# Acum, să trecem la treabă!

## **Ce veți avea de făcut mai exact?**

Prima dată va trebui să facem rost de materialele cu care vom lucra. Pentru a face asta deschidem:

- **Linia de comandă** - dacă avem Ubuntu
- **[Git bash](https://git-scm.com/download/win)** - dacă avem Windows
  Vom crea un folder, iar aici vom face **o clonă** la tot ceea ce v-am pregătit, folosind comanda:

`git clone REPO_HTTPS_ADDRESS`

În cazul nostru:

`REPO_HTTPS_ADDRESS = https://gitlab.com/MrMorningstarL/ac_back-end_2023_cod.git`

Acum, dacă navigăm în folderul de mai sus...

![enter image description here](https://media.giphy.com/media/bZBmitwUwKtDa/giphy.gif)

...vom găsi un sub-folder nou, cu toate fișierele din acest **repository**.
Acolo regăsim un fișier ajutător, numit **utils.js**, în care veți găsi niște mici-unelte care vă vor ajuta în rezolvarea problemelor, cât și un fișier **server.js** în care veți lucra. 

# Prima oprire:

![](https://media.giphy.com/media/yIxLCtwz63eKY/giphy.gif)

Anul acesta, ne-am gândit să ajutăm Teatrul Rapsodia să țină evidența actorilor săi. Din păcate, Croco a adormit la calculator lucrând și, căzând cu capul pe tastatură, a șters baza de date și avem nevoie de ajutorul vostru ca să reparăm situația.

## Ce veți avea de făcut?

![](https://media.giphy.com/media/o0vwzuFwCGAFO/giphy.gif)

- Accesați folderul tocmai clonat, apoi accesați **server.js**
- În fereastra de bash rulați comanda:
  `code.`
  O să se deschidă Visual Studio Code automat unde veți găsi fișierul în care veți lucra.

- (Pentru Windows) Apăsați butonul "Start All"
- (Pentru MacOS) Rulați modulele **Apache** și **MySQL** din XAMPP Control Panel
- Se creează baza de date **siscit_back_end** cu **utf8_general_ci (utf8_mb3_general_ci pentru Laragon) la Collation**, nu vă speriați, este extrem de simplu și vă vom arăta cum să faceți asta ;)
- Deschidem terminalul în Visual Studio Code și rulăm următoarele comenzi:

  `npm install`

  `npm install -g nodemon`

- Pentru a porni serverul folosiți comanda: `npx nodemon` sau `npm start`

- Din Visual Studio Code, accesați fișierul **server.js** și analizați problemele create de Croco, dar și cerințele care au venit de la directorul sălii de teatru.
- Atunci când în terminal nu se va mai află nicio eroare, accesați în browserul vostru **localhost:1234**
- În momentul în care datele actorilor vor fi introduse fără erori în tabelele din baza de date **ac_backend_2023**, va putea începe spectacolul.

  > Pentru testare încercați să aveți deschis terminalul din Studio Code. De asemenea, după modificări, nu uitați să salvați!!! **Ctrl + S**

Păi bine mă nene mă, dar ne spui odată ce avem de făcut?
![](https://media.giphy.com/media/HtYsYjPsw1nVu/giphy.gif)

Pentru azi, aveți următoarele task-uri (toate operațiile trebuie să fie realizate atunci când este apelată ruta localhost:1234/ac din browser):

- creați obiectele pentru cel puțin 5 actori

**Notă**: Actorul are nume(string), prenume(string), sex(string), rol(string), salariu(integer), dataNasterii(date) și esteDebutant(boolean). 
**ID-ul nu trebuie introdus! Acesta se introduce în mod automat la inserarea în baza de date.**

- folosind utils, inserați aceste obiecte în baza de date

**Notă**: pentru a vedea modificările din baza de date, trebuie dat refresh la tabelă.

- folosind utils, returnați actorii din baza de date și afișați-i în consolă
- folosind utils, modificați 2 roluri 
- folosind utils, scădeți salariile actorilor masculini
- folosind utils, afișați debutantul cu cel mai mare salariu
- folosind utils, calculați media de salariu și afișați în browser această valoare
- folosind utils, împărțiți actorii în cei cu salariu peste medie și sub medie și afișați în terminal aceste categorii
- folosind utils, schimbați rolul celui cu salariu cel mai mic
- folosind utils, creați o secțiune de credite, după formatul “nume-prenume-....(câte puncte doriți)...-rol”
- folosind utils, afișați toți actorii feminini, apoi pe cei masculini
- folosind utils, sortați actorii după varsta, după salariu și afișați cele două sortări




Baftă la codat!
![](https://media.giphy.com/media/Ym7JUEUTAyFSTJLc4K/giphy.gif)

  

**Ai îndeplinit cu succes toți pașii?**

![enter image description here](https://media.giphy.com/media/25722M3fXeVKJKw5jf/giphy.gif)

**Felicitări!** Atunci hai să îi anunțăm și pe ceilalți că problema a fost rezolvată și să urcăm codul sursă undeva... 

![enter image description here](https://media.giphy.com/media/6MwA2VkzK5uKF2izwd/giphy.gif)

** Ștergeți fișierul node_modules, iar fișierul creat de voi inițial ("ac-back-NUME-PRENUME") îl veți pune pe stick sau îl puteți trimite la următoarea adresă de e-mail:**

`alexandruion2003.ai@gmail.com - Leonard Ion , Lead divizie Back-End.`

Felicitări, ai trecut cu bine și de acest obstacol!

**Următorul popas vine în curând...**
![](https://media.giphy.com/media/FoH28ucxZFJZu/giphy.gif)