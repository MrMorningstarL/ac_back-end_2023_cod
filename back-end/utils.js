const {Sequelize, DataTypes} = require('sequelize');

const database = new Sequelize("ac_backend_2023", "root", "", {
    dialect: "mysql",
    host: "localhost",
    logging: false,
    define: {
        charset: "utf8",
        collate: "utf8_general_ci",
        timestamps: true,
    },
});


const actorDb = database.define(
    "actor",
    {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        nume: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        prenume: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        sex: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        rol: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        salariu: {
            type: DataTypes.BIGINT,
            allowNull: false,
        },
        dataNasterii: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        esteDebutant: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        }
    },
    {
        freezeTableName: true,
    }
);


/**
 * Functia creaza tabelul actorilor in baza de date si il goleste daca exista deja.
 * @return
 * Daca operatia a mers, functia va returna 'succes!'
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function resetDatabase() {
    return database
        .sync({force: true})
        .then(() => {
            return 'succes!'
        })
        .catch(reason => {
            return 'eroare'
        });
}


/**
 * Functia intoarce toti userii din baza de date
 * @return
 * Daca operatia a mers, va returna un array cu toti utilizatorii.
 * Daca operatia esueaza, va intoarce 'eroare!'
 */
function getAllActors() {
    return actorDb
        .findAll()
        .then(actors => {
            return actors.map(actor => actor.get());
        })
        .catch(() => {
            return 'eroare!';
        });
}


/**
 * Functia primeste ca parametru un id (numar intreg) si intoarce actorul cu id-ul mentionat.
 * @param {number} id
 * @return
 * Daca operatia a mers, functia va returna actorul cu id-ul mentionat
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function getActorById(id) {
    return actorDb
        .findByPk(id)
        .then(actor => {
            return actor.get();
        })
        .catch(() => {
            return 'eroare!';
        })
}


/**
 * Functia primeste un actor ca parametru pe care il insereaza in baza de date
 * @return
 * Daca operatia a mers, functia va returna 'succes!'
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function insertActor(actor) {
    return actorDb
        .create(actor)
        .then(() => {
            return 'succes!';
        })
        .catch((err) => {
            console.log(err);
            return 'eroare!';
        });
}


/**
 * Functia primeste 2 parametrii, primul este id-ul actorului care va fi modificat si al doilea este rolul (string) dorit pentru actor.
 * @param {number} id
 * @param {string} role
 * @return
 * Daca operatia a mers, functia va returna 'succes!'
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function updateRoleOfActor(id, role) {
    return actorDb
        .update({
            rol: role,
        }, {
            where: {
                id: id,
            }
        })
        .then(() => {
            return 'succes!';
        })
        .catch(() => {
            return 'eroare!';
        });
}


/**
 * Functia primeste 2 parametrii, primul este id-ul actorului care va fi modificat si al doilea este salariul (numar intreg) dorit pentru actor.
 * @param {number} id
 * @param {number} salary
 * @return
 * Daca operatia a mers, functia va returna 'succes!'
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function updateSalaryOfActor(id, salary) {
    return actorDb
        .update({
            salariu: salary,
        }, {
            where: {
                id: id,
            }
        })
        .then(() => {
            return 'succes!';
        })
        .catch(() => {
            return 'eroare!';
        });
}


/**
 * Functia primeste 2 parametrii, primul este id-ul actorului care va fi modificat si al doilea este statutul de debutant (adevarat sau fals) dorit pentru actor.
 * @param {number} id
 * @param {boolean} debutante
 * @return
 * Daca operatia a mers, functia va returna 'succes!'
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function updateDebutanteOfActor(id, debutante) {
    return actorDb
        .update({
            esteDebutant: debutante,
        }, {
            where: {
                id: id,
            }
        })
        .then(() => {
            return 'succes!';
        })
        .catch(() => {
            return 'eroare!';
        });
}


/**
 * Functia primeste ca parametru un id (numar intreg) si sterge actorul cu id-ul mentionat.
 * @return
 * Daca operatia a mers, functia va returna 'succes!'
 * Daca operatia esueaza, functia va intoarce 'eroare!'
 */
function deleteActorById(id) {
    return actorDb
        .destroy({
            where: {
                id: id,
            }
        })
        .then(() => {
            return 'succes!';
        })
        .catch(() => {
            return 'eroare!';
        })
}


module.exports = {
    resetDatabase,
    getAllActors,
    getActorById,
    insertActor,
    updateRoleOfActor,
    updateSalaryOfActor,
    updateDebutanteOfActor,
    deleteActorById,
}